package de.oszimt.vysioneer.common;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;

import java.util.Comparator;
import java.util.UUID;

public class ClientSettings {

    // Display Name
    SimpleStringProperty displayName;

    // Server list
    private ObservableList<VyServerEntry> serverList;


    public ClientSettings() {
        this.displayName = new SimpleStringProperty();
        this.serverList = FXCollections.observableArrayList();
    }


    public String getDisplayName() {
        return displayName.get();
    }

    public void setDisplayName(String displayName) {
        this.displayName.set(displayName);
    }

    public SimpleStringProperty displayNameProperty() {
        return displayName;
    }


    public ObservableList<VyServerEntry> getServerList() {
        return serverList;
    }

    public SortedList getSortedServerList() {
        return serverList.sorted(Comparator.comparing(VyServerEntry::getDisplayName));
    }

    public String generateRandomClient() {
        return "Client-" + UUID.randomUUID().toString().substring(0, 7);
    }

}
