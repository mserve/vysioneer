package de.oszimt.vysioneer.connector;

import org.junit.After;
import org.junit.Test;

import java.net.InetSocketAddress;

import static org.junit.Assert.assertNotNull;

public class VysioneerProxyTest {

    @Test
    public void addConnection() {
        VysioneerProxy proxy = VysioneerProxy.getInstance();
        assertNotNull(proxy);

        proxy.addConnection(new InetSocketAddress("localhost", 8888), new InetSocketAddress("localhost", 9999));

    }

    @Test
    public void closeConnection() {
    }

    @Test
    public void checkConnection() {
    }


    @After
    public void closeProxy() {
        VysioneerProxy proxy = VysioneerProxy.getInstance();
        proxy.exit();
    }
}