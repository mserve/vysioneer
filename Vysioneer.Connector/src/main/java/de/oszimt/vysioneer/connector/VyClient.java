package de.oszimt.vysioneer.connector;

import io.netty.channel.ChannelHandlerContext;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.UUID;

public class VyClient {

    private final String uniqueId;
    // Channel handler context is not serializable
    private transient final ChannelHandlerContext cxt;
    private String displayName;

    // Attached display
    private transient VyDisplay display = null;
    private boolean showing;


    public VyClient(ChannelHandlerContext cxt) {
        // Generate a random ID on th fly
        this("", cxt);
    }


    public VyClient(String uniqueId, ChannelHandlerContext cxt) {
        // Generate UUID on the fly, if not given
        if (uniqueId == null || uniqueId.isEmpty()) {
            uniqueId = VyClient.generateUniqueId();
        }
        this.uniqueId = uniqueId;
        this.cxt = cxt;
    }

    public static String generateUniqueId() {
        return UUID.randomUUID().toString();
    }

    public static String encode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String decode(String s) {
        try {
            return URLDecoder.decode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getUniqueId() {
        return uniqueId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
        if (this.display != null) {
            this.display.updateClient(this);
        }
    }

    public ChannelHandlerContext getChannelContext() {
        return cxt;
    }

    public void attachDisplay(VyDisplay newDisplay) {
        if (newDisplay != null)
            if (this.display != null) {
                this.display.removeClient(this);

                newDisplay.addClient(this);
                this.display = newDisplay;
            }
    }

    public void removeDisplay() {
        this.cxt.write("DISCONNECT\r\n");
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;

        if (o == this)
            return true;

        if (!o.getClass().equals(getClass()))
            return false;

        VyClient that = (VyClient) o;

        return this.uniqueId.equals(that.getUniqueId());
    }

    @Override
    public int hashCode() {
        return this.uniqueId.hashCode();
    }

    public void stopShowing() {
        cxt.write("STOP\r\n");
        this.showing = false;
    }

    public void startShowing() {
        this.showing = true;
        // We need some more magic here to establish the connection
    }
}
