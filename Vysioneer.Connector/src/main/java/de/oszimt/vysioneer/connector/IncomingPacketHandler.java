package de.oszimt.vysioneer.connector;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import org.apache.log4j.Logger;

import java.net.InetSocketAddress;
import java.util.Map;

public class IncomingPacketHandler extends SimpleChannelInboundHandler<DatagramPacket> {

    private final Logger logger = Logger.getLogger(IncomingPacketHandler.class);

    private Map<InetSocketAddress, InetSocketAddress> connectionMap;

    public IncomingPacketHandler(Map<InetSocketAddress, InetSocketAddress> connectionMap) {
        this.connectionMap = connectionMap;
        logger.info("Listening handler established");
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DatagramPacket msg) {
        // ok, now we need to lookup from whom to whom...
        InetSocketAddress target = connectionMap.get(msg.sender());
        logger.debug("Packet received at port " + msg.recipient().getPort() + "from ." + msg.sender().getAddress().getHostAddress() + ":" + msg.sender().getPort());

        // and send something
        if (target != null) {
            ctx.writeAndFlush(new DatagramPacket(msg.content(), target));
        }


    }
}
