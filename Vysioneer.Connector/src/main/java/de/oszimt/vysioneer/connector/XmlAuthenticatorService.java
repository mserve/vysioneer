package de.oszimt.vysioneer.connector;

import org.apache.commons.configuration2.BaseHierarchicalConfiguration;
import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class XmlAuthenticatorService implements AuthenticatorService {

    private static XmlAuthenticatorService instance = null;
    private final Logger logger = Logger.getLogger(XmlAuthenticatorService.class);
    private HashMap<String, String> authKeyMap;

    private XmlAuthenticatorService() {
        authKeyMap = new HashMap<>();
        // Read in the XML file and parse it into the map
        XMLConfiguration config;
        FileBasedConfigurationBuilder<XMLConfiguration> builder;
        Configurations configs = new Configurations();
        try {
            builder = configs.xmlBuilder(Main.class.getResource("displays.xml"));
            config = builder.getConfiguration();
            // Load servers
            List displays = config.configurationsAt("displaylist.display");
            logger.info("Found " + displays.size() + " displays in displays.xml");
            for (Iterator it = displays.iterator(); it.hasNext(); ) {
                // Load node
                BaseHierarchicalConfiguration display = (BaseHierarchicalConfiguration) it.next();
                // Get info
                String displayName = display.getString("displayname");
                String authKey = display.getString("authkey", "");
                if (displayName != null && !displayName.isEmpty()) {
                    authKeyMap.put(displayName, authKey);
                    logger.info("Adding display " + displayName);
                } else {
                    logger.warn("Skipping empty display");
                }
            }
        } catch (Exception e) {
            // Log a warning
            logger.error("Could not read displays.xml", e);
        }
    }

    public static XmlAuthenticatorService getInstance() {
        if (instance == null) {
            instance = new XmlAuthenticatorService();
        }
        return instance;
    }

    public boolean authenticate(String displayName, String authKey) {
        String authKeyWanted = authKeyMap.get(displayName);
        if (authKeyWanted == null) {
            logger.warn("Could not auth display \"" + displayName + "\" - not found");
            return false;
        }
        if (authKeyWanted.equals(authKey))
            return true;
        else {
            logger.warn("Could not auth display \"" + displayName + "\" - wrong key");
            return false;
        }
    }
}
