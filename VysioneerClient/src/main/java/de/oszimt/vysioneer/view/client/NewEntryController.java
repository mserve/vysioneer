package de.oszimt.vysioneer.view.client;

import de.oszimt.vysioneer.Main;
import de.oszimt.vysioneer.common.PreferenceStore;
import de.oszimt.vysioneer.common.VyServerEntry;
import de.oszimt.vysioneer.view.BaseController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.util.converter.IntegerStringConverter;

import java.net.InetSocketAddress;
import java.net.URL;
import java.util.ResourceBundle;

public class NewEntryController extends BaseController {

    @FXML
    private Button btnCancel;

    @FXML
    private Button btnAdd;

    @FXML
    private TextField tfdAddress;

    @FXML
    private TextField tfdPort;

    @FXML
    void onBtnAdd(ActionEvent event) {
        if (tfdAddress.getText().trim().isEmpty()) {
            // TO DO: Alert
            return;
        }

        // create entry object
        VyServerEntry entry = new VyServerEntry();

        // Parse port
        int port = 8090;
        try {
            port = (Integer.parseInt(tfdPort.getText().trim()));
        } catch (NumberFormatException e) {
            Main.logger.info("Could not parse port, using default value");
        }

        // Set values
        entry.setDisplayName(tfdAddress.getText().trim());
        entry.setRemoteAddress(InetSocketAddress.createUnresolved(tfdAddress.getText().trim(), port));

        // Add entry to list
        PreferenceStore.getInstance().getClientSettings().getServerList().add(entry);
        btnCancel.getScene().getWindow().hide();
    }

    @FXML
    void onBtnCancel(ActionEvent event) {
        btnCancel.getScene().getWindow().hide();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // force the field to be numeric only
        tfdPort.textFormatterProperty().setValue(new TextFormatter(new IntegerStringConverter()));

        /*
        tfdPort.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    tfdPort.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });*/
    }
}
