package de.oszimt.vysioneer.view.server;

import de.oszimt.vysioneer.common.PreferenceStore;
import de.oszimt.vysioneer.common.ServerSettings;
import de.oszimt.vysioneer.common.VyClientEntry;
import de.oszimt.vysioneer.view.BaseController;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.text.Text;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ResourceBundle;

public class MainController extends BaseController {

    @FXML
    private ListView<VyClientEntry> lstServer;

    @FXML
    private SplitMenuButton btnDisplayClient;

    @FXML
    private MenuItem mnuDisconnect;

    @FXML
    private MenuItem mnuDetails;

    @FXML
    private MenuItem mnuSettings;

    @FXML
    private Text tfdServerAddress;

    @FXML
    private Text tfdServerPin;

    // Observable list of clients
    private ObservableList<VyClientEntry> oListClients;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Get settings
        ServerSettings settings = PreferenceStore.getInstance().getServerSettings();

        // Bind properties
        tfdServerAddress.textProperty().bind(settings.displayNameProperty());
        // Format PIN
        DecimalFormat df = new DecimalFormat("0");
        df.setGroupingSize(4);
        df.setGroupingUsed(true);
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setGroupingSeparator(' ');
        df.setDecimalFormatSymbols(dfs);

        tfdServerPin.textProperty().bind(Bindings.createStringBinding(() ->
                df.format(settings.currentCodeProperty().get()), settings.currentCodeProperty())
        );
        oListClients = settings.getSortedClientList();
    }

    @FXML
    void onBtnDisplayClient(ActionEvent event) {

    }

    @FXML
    void onMnuDetails(ActionEvent event) {

    }

    @FXML
    void onMnuDisconnect(ActionEvent event) {

    }

    @FXML
    void onMnuSettings(ActionEvent event) {

    }
}
