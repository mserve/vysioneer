package de.oszimt.vysioneer.common;

import de.oszimt.vysioneer.Main;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.util.Duration;

import java.util.Comparator;
import java.util.Random;
import java.util.UUID;

public class ServerSettings {

    private final SimpleStringProperty serverAddress;
    private final SimpleIntegerProperty port;
    private final SimpleStringProperty displayName;

    private final SimpleIntegerProperty currentCode;

    // Server list
    private ObservableList<VyClientEntry> clientList;


    public ServerSettings() {
        // Setup properties
        this.displayName = new SimpleStringProperty();
        this.port = new SimpleIntegerProperty();
        this.serverAddress = new SimpleStringProperty();
        this.clientList = FXCollections.observableArrayList();

        // Setup code and timer
        Random r = new Random();

        this.currentCode = new SimpleIntegerProperty(r.nextInt(99999999));


        Timeline codeGenerator = new Timeline(new KeyFrame(Duration.seconds(10),
                event -> {
                    int code = r.nextInt(99999999);
                    Main.logger.info("New code is " + code);
                    currentCodeProperty().set(code);
                }));
        codeGenerator.setCycleCount(Timeline.INDEFINITE);
        codeGenerator.play();
    }


    public String getDisplayName() {
        return displayName.get();
    }

    public void setDisplayName(String displayName) {
        this.displayName.set(displayName);
    }

    public SimpleStringProperty displayNameProperty() {
        return displayName;
    }

    public String getServerAddress() {
        return serverAddress.get();
    }

    public void setServerAddress(String serverAddress) {
        this.serverAddress.set(serverAddress);
    }

    public SimpleStringProperty serverAddressProperty() {
        return serverAddress;
    }

    public int getPort() {
        return port.get();
    }

    public void setPort(int port) {
        this.port.set(port);
    }

    public SimpleIntegerProperty portProperty() {
        return port;
    }

    public int getCurrentCode() {
        return currentCode.get();
    }

    public void setCurrentCode(int currentCode) {
        this.currentCode.set(currentCode);
    }

    public SimpleIntegerProperty currentCodeProperty() {
        return currentCode;
    }

    public ObservableList<VyClientEntry> getClientList() {
        return clientList;
    }

    public SortedList getSortedClientList() {
        return clientList.sorted(Comparator.comparing(VyClientEntry::getDisplayName));
    }

    public String generateRandomName() {
        return "Server-" + UUID.randomUUID().toString().substring(0, 7);
    }

}
