package de.oszimt.vysioneer.connector;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(Parameterized.class)
public class DisplayLookupServiceTest {

    @Parameterized.Parameter(0)
    public String displayName;
    private DisplayLookupService dls;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{{"2203-L01"}, {"2429-L01"}};
        return Arrays.asList(data);
    }


    @Before
    public void setupDisplayLookupService() {
        this.dls = new DisplayLookupService();
        VyDisplay display = new VyDisplay(displayName);
        dls.getDisplayMap().put(displayName, display);
    }

    @Test
    public void registerDisplay() {
        VyDisplay display = new VyDisplay("1234-Test");
        dls.registerDisplay(display);

        // Check if display is registered
        assertNotNull(dls.getDisplayMap().get(displayName));
    }

    @Test
    public void displayLookup() {
        // Should work
        assertNotNull(dls.displayLookup(displayName));
        // Should fail
        assertNull(dls.displayLookup("xx" + displayName));
    }


    @Test(expected = RuntimeException.class)
    public void registerDisplayNull() {
        // Check if error
        dls.registerDisplay(null);
    }

    @Test(expected = RuntimeException.class)
    public void registerDisplayEmptyName() {
        // Check if error
        dls.registerDisplay(new VyDisplay(""));
    }


    @Test
    public void removeDisplay() {
        VyDisplay d = dls.getDisplayMap().get(displayName);
        assertNotNull(d);
        dls.removeDisplay(d);
        assertNull(dls.getDisplayMap().get(displayName));

        // Re-Register for other tests
        dls.registerDisplay(d);
    }
}