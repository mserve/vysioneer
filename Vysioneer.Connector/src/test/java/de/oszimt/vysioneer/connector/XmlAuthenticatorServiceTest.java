package de.oszimt.vysioneer.connector;

import org.junit.Test;

import static org.junit.Assert.*;

public class XmlAuthenticatorServiceTest {

    @Test
    public void getInstance() {
        XmlAuthenticatorService as = XmlAuthenticatorService.getInstance();
        // Check if we actually got an instance
        assertTrue(as.getClass() == XmlAuthenticatorService.class);
        // Check if we got the *same* instance
        XmlAuthenticatorService anotherAs = XmlAuthenticatorService.getInstance();
        assertSame(as, anotherAs);

    }

    @Test
    public void authenticate() {
        XmlAuthenticatorService as = XmlAuthenticatorService.getInstance();
        // We need to get the as
        assertNotNull(as);
        // Authenticate should work on this one
        assertTrue(as.authenticate("2203-L01", "demo2203"));
        // Authentication should fail - not found
        assertFalse(as.authenticate("xx-2203-L01", "demo2203"));
        // Authentication should fail - wrong credentials
        assertFalse(as.authenticate("2203-L01", "xx-demo2203"));
        // Authentication should fail - no credentials
        assertFalse(as.authenticate("2203-L01", ""));
    }
}