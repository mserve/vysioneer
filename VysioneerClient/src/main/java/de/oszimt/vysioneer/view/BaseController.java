package de.oszimt.vysioneer.view;

import de.oszimt.vysioneer.Main;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public abstract class BaseController implements Initializable {

    // All tasks to be done AFTER fxml loading keep to actual controllers
    @Override
    public abstract void initialize(URL location, ResourceBundle resources);

    protected BaseController loadModalWindow(String fxmlPath, String title) throws IOException {
        return loadWindow(fxmlPath, title, false, true, false);

    }

    protected BaseController loadModalWindow(String fxmlPath, String title, boolean isResizeable) throws IOException {
        return loadWindow(fxmlPath, title, isResizeable, true, false);
    }

    protected BaseController loadWindow(String fxmlPath, String title, boolean isResizeable, boolean isModal, boolean deferShow) throws IOException {

        // Load FXML
        // Create new root, stage and scene
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(Main.class.getResource(fxmlPath));
        Parent root = fxmlLoader.load();

        // Create stage
        Stage subStage = new Stage();
        Scene scene = new Scene(root);
        subStage.setScene(scene);

        // Set stage properties
        if (isModal)
            subStage.initModality(Modality.APPLICATION_MODAL);

        subStage.setResizable(isResizeable);
        subStage.setTitle(title);
        subStage.getIcons().add(new Image(Main.class.getResourceAsStream("view/vy_icon.png")));

        // Show the stage if requested
        if (!deferShow)
            subStage.show();

        // Return the controller
        return fxmlLoader.getController();
    }

    public Optional<ButtonType> showAlert(String title, String header, String content) {
        return showAlert(title, header, content, Alert.AlertType.ERROR);
    }


    public Optional<ButtonType> showAlert(String title, String header, String content, Alert.AlertType type) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);

        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(
                new Image(Main.class.getResourceAsStream("view/vy_icon.png")));


        return alert.showAndWait();
    }
}
