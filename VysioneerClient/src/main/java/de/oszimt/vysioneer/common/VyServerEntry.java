package de.oszimt.vysioneer.common;

import java.net.InetSocketAddress;
import java.util.Date;

public class VyServerEntry {

    private String displayName;
    private InetSocketAddress remoteAddress;
    private Date lastSeen;


    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public InetSocketAddress getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(InetSocketAddress remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public Date getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Date lastSeen) {
        this.lastSeen = lastSeen;
    }

    @Override
    public String toString() {
        return displayName;
    }

}
