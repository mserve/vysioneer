package de.oszimt.vysioneer.view.client;

import de.oszimt.vysioneer.common.PreferenceStore;
import de.oszimt.vysioneer.view.BaseController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;


public class SettingsController extends BaseController {
    @FXML
    private Button btnCancel;

    @FXML
    private Button btnChange;

    @FXML
    private TextField tfdUsername;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Load data from settings (do not use property, as we only want do update on confirm
        tfdUsername.setText(PreferenceStore.getInstance().getClientSettings().getDisplayName());

        // Limit length to 64 chars.
        tfdUsername.setTextFormatter(new TextFormatter<String>(change ->
                change.getControlNewText().length() <= 64 ? change : null));
    }

    @FXML
    void onBtnCancel(ActionEvent event) {
        btnCancel.getScene().getWindow().hide();
    }

    @FXML
    void onBtnChange(ActionEvent event) {
        if (tfdUsername.getText().trim().isEmpty()) {
            Optional<ButtonType> result = showAlert("Leerer Benutzername", "Benutzername fehlt.", "Bitte geben Sie einen Benutzernamen ein.");
            return;
        }

        PreferenceStore.getInstance().getClientSettings().setDisplayName(tfdUsername.getText());
        btnChange.getScene().getWindow().hide();

    }
}
