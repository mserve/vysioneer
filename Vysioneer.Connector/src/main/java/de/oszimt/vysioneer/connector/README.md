Server: HELLO <<Servername>>
Client: HELLO <<Username>>

--
Client: LIST
Server: ((Liste))

---
Client: CONNECT <<Displayname>> <<Pin>>

Server: +OK (oder) -ERR

----- Wenn Pin OK und Lehrer das Display „anfordert“:
Server: SHOW <<IP>> <<PORT>> <<Zufälliger Key>>
Client: +OK

----- Wenn Display auf Lehrer-PC angefordert
Client: STOP
Server +OK

Server: STOP
Client: +OK

----- Allgemein
Server: PING
Client: +OK

Client: PING
Server: +OK

Client: EXIT
Server: +OK Goodbye

Server: EXIT
Client: +OK Goodbye
