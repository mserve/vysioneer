package de.oszimt.vysioneer.connector;

import io.netty.channel.ChannelHandlerContext;

import java.util.HashMap;
import java.util.Map;

public class VyDisplay {

    private String displayName;

    // Use if not set yet
    private String pin = null;
    private String lastPin = null;

    private Map<String, VyClient> clientMap = new HashMap<>();

    private ChannelHandlerContext ctx;
    private VyClient currentDisplayShowing;


    public VyDisplay(String displayName) {
        if (displayName.isEmpty()) {
            throw new RuntimeException("Display name cannot be empty!");
        }
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }


    public void connectChannel(ChannelHandlerContext ctx) {

        if (ctx == null) {
            throw new NullPointerException();
        }

        // Check if we need to deregister
        if (this.ctx != null) {
            // Deregister old channel
            ctx.write("QUIT");
            ctx.flush();
            ctx.close();
        }
        // Register new context
        this.ctx = ctx;
    }

    public void addClient(VyClient vc) {
        if (this.clientMap.containsKey(vc.getUniqueId())) {
            return;
        }
        this.ctx.write("NOTIFY ADD " + vc.getUniqueId() + " " + VyClient.encode(vc.getDisplayName()) + "\r\n");
        this.ctx.flush();
        this.clientMap.put(vc.getUniqueId(), vc);
        vc.attachDisplay(this);
    }

    public void removeClient(VyClient vc) {
        // Notify via channel
        if (!this.clientMap.containsKey(vc.getUniqueId())) {
            return;
        }
        this.ctx.write("NOTIFY REMOVE " + vc.getUniqueId() + "\r\n");
        this.ctx.flush();
        this.clientMap.remove(vc.getUniqueId());
        vc.removeDisplay();
    }

    public void closeDisplay() {
        for (Map.Entry<String, VyClient> entry : clientMap.entrySet()) {
            entry.getValue().removeDisplay();
        }
        ctx.write("QUIT\r\n");
        ctx.close();
    }

    public boolean checkPin(String pin) {
        // Pin is not null nor empty and equals to current or last pin
        // Last pin is used to reduce danger of glitches
        return (pin != null) && (!pin.isEmpty()) && (pin.equals(this.pin) || pin.equals(this.lastPin));
    }

    public void setPin(String pin) {
        // Is pin valid?
        if (pin != null && !pin.isEmpty()) {
            // Yes > change last pin and pin
            this.lastPin = this.pin;
            this.pin = pin;
        }
    }

    public boolean hasClient(String clientId) {
        return this.clientMap.containsKey(clientId);
    }


    public void startShowing(VyClient display) {
        // TODO the magic logic to really start the SHOWING ;-)
        // i.e. call the proxy service to establish the showing part
        this.currentDisplayShowing = display;
        display.startShowing();
    }

    public boolean isShowing() {
        return this.currentDisplayShowing != null;
    }

    public boolean isShowing(VyClient display) {
        return isShowing() && (this.currentDisplayShowing.equals(display));
    }

    public void startShowing(String clientId) {
        if (this.hasClient(clientId)) {
            startShowing(this.clientMap.get(clientId));
        } else {
            throw new RuntimeException("Cannot show client - not connected");
        }

    }

    public void stopShowing() {
        if (this.isShowing()) {
            this.currentDisplayShowing.stopShowing();
            ctx.write("STOP\r\n");
            this.currentDisplayShowing = null;
        }
    }

    public void updateClient(VyClient vyClient) {
        clientMap.put(vyClient.getUniqueId(), vyClient);
        this.ctx.write("NOTIFY UPDATE " + vyClient.getUniqueId() + " " + VyClient.encode(vyClient.getDisplayName()));
    }
}
