package de.oszimt.vysioneer.connector;


import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.ReferenceCountUtil;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Scanner;

public class VysioneerServerHandler extends SimpleChannelInboundHandler<String> {

    // keep track of display or client
    private VyDisplay display = null;
    private VyClient client = null;

    private DisplayLookupService dls = new DisplayLookupService();


    // keep track of internal state
    private VyMode mode = VyMode.UNKNOWN;
    private VyServerState state = VyServerState.HANDSHAKE;

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) {
        try {
            // Get command word
            Scanner scanner = new Scanner(msg);

            String cmd = scanner.next().toUpperCase();

            if (cmd.isEmpty()) {
                sendOk(ctx, "NOOP");
                return;
            }

            if (cmd.equals("NOOP")) {
                sendOk(ctx);
                return;
            }

            if (cmd.equals("EXIT")) {
                // Handle exit

                // Close the channel
                sendOk(ctx);
                ctx.close();
                return;
            }


            // Handshake message - now, we require a hello
            switch (state) {
                case HANDSHAKE:
                    if (cmd.equals("HELLO"))
                        handleHandshake(ctx, scanner);
                    else
                        sendError(ctx, "HELLO expected");
                    // We parsed and sent a response > done
                    return;
                case CONNECTED:
                    switch (this.mode) {
                        case DISPLAY:
                            switch (cmd) {
                                case "PIN":
                                    String newPin = URLDecoder.decode(scanner.next(), "UTF-8");
                                    this.display.setPin(newPin);
                                    break;
                                case "SHOW":
                                    // Display wants to show some client
                                    String clientId = scanner.next();
                                    if (this.display.hasClient(clientId)) {
                                        this.display.startShowing(clientId);
                                    } else {
                                        sendError(ctx, "Client " + clientId + " not connected");
                                        return;
                                    }
                                    break;
                                case "STOP":
                                    this.display.stopShowing();
                                    sendOk(ctx);
                                    return;
                            }
                            break;
                        case CLIENT:
                            switch (cmd) {
                                case "CONNECT":
                                    String displayName = URLDecoder.decode(scanner.next(), "UTF-8");
                                    String pin = scanner.next();
                                    // Try to connect...
                                    VyDisplay d = dls.displayLookup(displayName);
                                    if (d == null) {
                                        sendError(ctx, "Display " + displayName + "not found");
                                        return;
                                    }
                                    if (!d.checkPin(pin)) {
                                        sendError(ctx, "Wrong pin for " + displayName);
                                        return;
                                    }
                                    d.addClient(this.client);
                                    break;
                                case "STOP":
                                    this.client.stopShowing();
                                    sendOk(ctx);
                                    return;
                                case "DISCONNECT":
                                    this.client.removeDisplay();
                                    sendOk(ctx);
                                    return;
                            }
                            break;
                    }
                    break;
                case SHOWING:
                    switch (this.mode) {
                        case DISPLAY:
                            break;
                        case CLIENT:
                            break;
                    }

            }

            // Ok, we handled all our commands. If we land here,
            // there was no response - send an error then
            sendError(ctx, "Unknown command");
            return;
        } catch (Exception e) {
            sendError(ctx, "Unable to parse command");
            // Do something with msg
        } finally {
            ReferenceCountUtil.release(msg);
        }
    }

    private void handleHandshake(ChannelHandlerContext ctx, Scanner scanner) throws UnsupportedEncodingException {

        String mode = scanner.next();
        switch (mode.toUpperCase()) {
            case "CLIENT":
                this.mode = VyMode.CLIENT;
                break;
            case "DISPLAY":
                this.mode = VyMode.DISPLAY;
                break;
            default:
                sendError(ctx, "Unknown " + mode + " -MODE must be CLIENT or DISPLAY");
                return;
        }

        String displayName = URLDecoder.decode(scanner.next(), "UTF-8");

        switch (this.mode) {

            case DISPLAY:
                String authKey = scanner.next();
                // Handle login
                if (!XmlAuthenticatorService.getInstance().authenticate(displayName, authKey)) {
                    sendError(ctx, "Could not authenticate display");
                    return;
                }
                // Prepare Display

                // Check if display already is added
                VyDisplay d = dls.displayLookup(displayName);

                // Ok, not yet connected, create one
                if (d == null) {
                    d = new VyDisplay(displayName);
                }

                addDisplay(ctx, d);
                break;
            case CLIENT:
                // Handle client
                VyClient c = new VyClient("", ctx);
                c.setDisplayName(displayName);
                addClient(ctx, c);
                break;
        }
        this.state = VyServerState.CONNECTED;
        sendOk(ctx, "Welcome, " + displayName);
        return;

    }

    private void addDisplay(ChannelHandlerContext ctx, VyDisplay d) {
        d.connectChannel(ctx);
        this.display = d;
    }

    private void addClient(ChannelHandlerContext ctx, VyClient c) {
        if (this.display != null) {
            this.display.addClient(c);
        }
    }

    private void sendError(ChannelHandlerContext ctx, String message) {
        ctx.write("-ERR " + message + "\r\n");
        ctx.flush();
    }

    private void sendOk(ChannelHandlerContext ctx, String message) {
        ctx.write("+OK " + message + "\r\n");
        ctx.flush();
    }

    private void sendOk(ChannelHandlerContext ctx) {
        sendOk(ctx, "");
    }


    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        // TODO: Set proper HELLO name
        sendOk(ctx, "HELLO MeinServer");
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) { // (4)
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }
}
