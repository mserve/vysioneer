package de.oszimt.vysioneer.connector;

import java.util.HashMap;
import java.util.Map;

public class DisplayLookupService {
    private final Map<String, VyDisplay> vyDisplayMap;

    public DisplayLookupService() {
        vyDisplayMap = new HashMap<>();
    }

    public VyDisplay displayLookup(String key) {
        return vyDisplayMap.get(key);
    }

    public void registerDisplay(VyDisplay display) {
        if (display == null) {
            throw new RuntimeException("Display may not be null!");
        }
        if (display.getDisplayName().isEmpty()) {
            throw new RuntimeException("Display name must not be empty!");
        }
        // Register the display
        vyDisplayMap.put(display.getDisplayName(), display);
    }

    public void removeDisplay(VyDisplay display) {
        vyDisplayMap.remove(display.getDisplayName());
    }

    public Map<String, VyDisplay> getDisplayMap() {
        return vyDisplayMap;
    }
}
