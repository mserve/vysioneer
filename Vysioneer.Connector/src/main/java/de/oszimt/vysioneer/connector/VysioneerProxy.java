package de.oszimt.vysioneer.connector;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import org.apache.log4j.Logger;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class VysioneerProxy {


    // Prepare instance
    private static VysioneerProxy instance = null;
    private final Logger logger = Logger.getLogger(VysioneerProxy.class);
    // starting from here, use the
    // next two ports
    private int portBase = 14242;
    // Keep track who should send to us (i.e. map the IPs and ports)
    // and look them up
    private Map<InetSocketAddress, InetSocketAddress> connectionMap = new ConcurrentHashMap<>();
    private ChannelFuture fRtcp;
    private ChannelFuture fRtp;

    public static VysioneerProxy getInstance() {
        if (instance == null) {
            instance = new VysioneerProxy();
            try {
                instance.run();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    public void addConnection(InetSocketAddress from, InetSocketAddress to) {
        logger.info("Adding connection between " + from.getAddress().getHostAddress() + " / " + from.getAddress().getHostAddress());

        // Data connection
        connectionMap.put(from, to);
        connectionMap.put(to, from);
        // Control connection
        InetSocketAddress fromControl = new InetSocketAddress(from.getAddress(), from.getPort() + 1);
        InetSocketAddress toControl = new InetSocketAddress(to.getAddress(), to.getPort() + 1);
        connectionMap.put(fromControl, toControl);
        connectionMap.put(toControl, fromControl);
    }

    public void closeConnection(InetSocketAddress addr) {
        InetSocketAddress to = connectionMap.get(addr);
        if (to != null) {
            connectionMap.remove(addr);
            connectionMap.remove(to);
            connectionMap.remove(new InetSocketAddress(addr.getAddress(), addr.getPort() + 1));
            connectionMap.remove(new InetSocketAddress(to.getAddress(), to.getPort() + 1));
        }
    }

    public boolean checkConnection(InetSocketAddress from, InetSocketAddress to) {
        return connectionMap.containsKey(from) || connectionMap.containsKey(to);
    }

    private void run() throws Exception {
        logger.info("Starting server at Port " + portBase + " / " + (portBase + 1));

        final NioEventLoopGroup group = new NioEventLoopGroup();
        try {
            final Bootstrap bRtp = new Bootstrap();
            bRtp.group(group).channel(NioDatagramChannel.class)
                    .option(ChannelOption.SO_BROADCAST, true)
                    .handler(new ChannelInitializer<NioDatagramChannel>() {
                        @Override
                        public void initChannel(final NioDatagramChannel ch) {
                            ChannelPipeline p = ch.pipeline();
                            p.addLast(new IncomingPacketHandler(connectionMap));
                        }
                    });


            final Bootstrap bRtcp = new Bootstrap();
            bRtcp.group(group).channel(NioDatagramChannel.class)
                    .option(ChannelOption.SO_BROADCAST, true)
                    .handler(new ChannelInitializer<NioDatagramChannel>() {
                        @Override
                        public void initChannel(final NioDatagramChannel ch) {
                            ChannelPipeline p = ch.pipeline();
                            p.addLast(new IncomingPacketHandler(connectionMap));
                        }
                    });

            // Bind and start to accept incoming connections.
            InetAddress address = InetAddress.getLocalHost();
            fRtp = bRtp.bind(address, portBase).sync();
            fRtcp = bRtcp.bind(address, portBase + 1).sync();


        } finally {
            logger.info("Server is listening at port " + portBase + " / " + (portBase + 1));
        }

    }


    public void exit() {
        try {
            logger.info("Closing server at port " + portBase + " / " + (portBase + 1));

            fRtp.channel().close().sync();
            fRtcp.channel().close().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            instance = null;
        }

    }
}