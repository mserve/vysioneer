package de.oszimt.vysioneer.connector;

public class VysioneerProxyService {

    private static VysioneerProxyService instance = null;


    private VysioneerProxyService() {

    }

    public static VysioneerProxyService getInstance() {
        if (instance == null) {
            instance = new VysioneerProxyService();
        }
        return instance;
    }

    // Add new proxy

    // Remove a proxy

    // Lookup proxy by client

    // Lookup proxy by server

    // Get next ports

}
