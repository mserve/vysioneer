package de.oszimt.vysioneer.connector;

public interface AuthenticatorService {

    // public static AuthenticatorService getInstance();
    boolean authenticate(String displayName, String authKey);

}
