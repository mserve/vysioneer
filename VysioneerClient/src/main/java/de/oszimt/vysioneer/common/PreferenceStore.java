package de.oszimt.vysioneer.common;

import de.oszimt.vysioneer.Main;
import javafx.collections.ListChangeListener;
import org.apache.commons.configuration2.BaseHierarchicalConfiguration;
import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.net.InetSocketAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class PreferenceStore {
    private static PreferenceStore instance = null;
    public final int MODE_SERVER = 1;
    public final int MODE_CLIENT = 2;

    private ServerSettings serverSettings;
    private ClientSettings clientSettings;

    private int mode = MODE_CLIENT;

    private XMLConfiguration config;
    private FileBasedConfigurationBuilder<XMLConfiguration> builder;

    private PreferenceStore() {
        // Try to load settings.xml
        Configurations configs = new Configurations();
        try {
            this.builder = configs.xmlBuilder(Main.class.getResource("settings.xml"));
            this.config = builder.getConfiguration();


            // Read mode
            String mode = config.getString("mode", "client");
            Main.logger.info("Reading settings, mode is " + mode);

            if (mode.equals("server")) {
                this.mode = MODE_SERVER;
                loadServerConfig();
            } else {
                this.mode = MODE_CLIENT;
                loadClientConfig();
            }

        } catch (ConfigurationException cex) {
            // Something went wrong
            Main.logger.error("Could not load settings.");
        }
    }

    public static PreferenceStore getInstance() {
        if (instance == null) {
            instance = new PreferenceStore();
        }
        return instance;
    }

    private void loadClientConfig() {
        // Log info
        Main.logger.info("Reading client configuration from " + config.getDocument().getLocalName());

        // Create new Client object and server list
        this.clientSettings = new ClientSettings();


        // Read display name and set the property
        clientSettings.setDisplayName(config.getString("client.name", clientSettings.generateRandomClient()));
        Main.logger.info("Display name is " + clientSettings.getDisplayName());


        // Iterate through all servers
        List servers = config.configurationsAt("client.serverlist.server");
        Main.logger.info("Found " + servers.size() + " servers in config");


        for (Iterator it = servers.iterator(); it.hasNext(); ) {
            // Load node
            BaseHierarchicalConfiguration server = (BaseHierarchicalConfiguration) it.next();
            // Get address
            String serverAddress = server.getString("address");

            // Skip if no address
            if (serverAddress == null) {
                Main.logger.warn("Invalid server entry in settings.xml");
            }

            Main.logger.info("Loading server info: " + serverAddress);


            // Get display name, use address by default
            String displayName = server.getString("displayname", serverAddress);

            // Get port
            int serverPort = server.getInt("port", 8090);

            // Get last seen
            Date lastSeen = server.get(Date.class, "lastseen");

            // Create new server object
            VyServerEntry vyServer = new VyServerEntry();
            vyServer.setDisplayName(displayName);
            vyServer.setRemoteAddress(InetSocketAddress.createUnresolved(serverAddress, serverPort));
            vyServer.setLastSeen(lastSeen);

            // Push to list
            clientSettings.getServerList().add(vyServer);

        }

        // Sort list by display name
        // clientSettings.getServerList().sort((o1, o2) -> o1.getDisplayName().compareTo(o2.getDisplayName()));

        // Add change listeners to properties
        clientSettings.getServerList().addListener((ListChangeListener<VyServerEntry>) c -> updateConfig());

        clientSettings.displayNameProperty().addListener(observable -> updateConfig());

    }

    private void loadServerConfig() {
        this.serverSettings = new ServerSettings();
        serverSettings.setDisplayName(config.getString("server.name", serverSettings.generateRandomName()));
        serverSettings.setServerAddress(config.getString("server.address", "localhost"));
        serverSettings.setPort(config.getInt("server.port", 8900));
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public ServerSettings getServerSettings() {
        return serverSettings;
    }

    public ClientSettings getClientSettings() {
        return clientSettings;
    }


    public void updateConfig() {
        Main.logger.info("Updating settings.xml...");

        // For some reasons, XMLConfiguration saves date in a format it cannot parse?!
        // Workarround: format date ourselves
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            config.setProperty("mode", (this.mode == MODE_SERVER ? "server" : "client"));
            if (this.mode == MODE_SERVER) {
                config.setProperty("server.name", serverSettings.getDisplayName());
                config.setProperty("server.address", serverSettings.getServerAddress());
                config.setProperty("server.port", serverSettings.getPort());

            } else {
                config.setProperty("client.name", clientSettings.getDisplayName());
                // Clear server list
                config.clearTree("client.serverlist");
                // Update from internal data
                for (VyServerEntry e : clientSettings.getServerList()) {
                    config.addProperty("client.serverlist.server(-1).address", e.getRemoteAddress().getHostName());
                    config.addProperty("client.serverlist.server.port", e.getRemoteAddress().getPort());
                    if (e.getLastSeen() != null)
                        config.addProperty("client.serverlist.server.lastseen", df.format(e.getLastSeen()));
                    config.addProperty("client.serverlist.server.displayname", e.getDisplayName());
                }
            }
            builder.save();
        } catch (ConfigurationException e) {
            Main.logger.error("Could not update settings.xml: ", e);
        }
    }
}
