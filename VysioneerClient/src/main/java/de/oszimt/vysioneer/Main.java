package de.oszimt.vysioneer;

import de.oszimt.vysioneer.common.PreferenceStore;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.log4j.Logger;


public class Main extends Application {

    public final static Logger logger = Logger.getLogger(PreferenceStore.class);


    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(Main.class.getResource("view/Splash.fxml"));
        Scene scene = new Scene(root, Color.TRANSPARENT);
        root.setStyle("-fx-background-color: transparent;");
        primaryStage.setScene(scene);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("view/vy_icon.png")));
        primaryStage.show();

        // Do some initialization
        PreferenceStore prefs = PreferenceStore.getInstance();

        if (prefs.getMode() == prefs.MODE_SERVER) {
            // load server
            root = FXMLLoader.load(Main.class.getResource("view/ServerMain.fxml"));
        } else {
            // load client
            root = FXMLLoader.load(Main.class.getResource("view/ClientMain.fxml"));
        }
        Stage mainStage = new Stage();
        mainStage.setTitle("Vysioneer");
        mainStage.setScene(new Scene(root));
        mainStage.getIcons().add(new Image(Main.class.getResourceAsStream("view/vy_icon.png")));
        mainStage.show();


        // Hide the splash window
        primaryStage.hide();
    }

    @Override
    public void stop() {
        Main.logger.info("Closing application.");
        // Save config
        PreferenceStore.getInstance().updateConfig();
    }
}
