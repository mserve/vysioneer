package de.oszimt.vysioneer.view.client;

import de.oszimt.vysioneer.common.VyServerEntry;
import de.oszimt.vysioneer.view.BaseController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class ShowEntryDetailsController extends BaseController {

    @FXML
    private Button btnClose;

    @FXML
    private Label lblServer;

    @FXML
    private Label lblAddress;

    @FXML
    private Label lblPort;

    @FXML
    private Label lblLastSeen;


    private VyServerEntry serverEntry;

    public void setServerEntry(VyServerEntry entry) {
        this.serverEntry = entry;
    }

    public void loadServerEntry(VyServerEntry entry) {
        // Link entry
        this.setServerEntry(entry);

        // Set the labels
        lblServer.setText(serverEntry.getDisplayName());

        if (serverEntry.getLastSeen() != null) {
            // Add Dateformatter
            SimpleDateFormat df = new SimpleDateFormat("d. MMMM yyyy / H:mm", Locale.getDefault());
            lblLastSeen.setText(df.format(serverEntry.getLastSeen()) + " Uhr");
        } else {
            lblLastSeen.setText("noch nicht verbunden");
        }

        lblAddress.setText(serverEntry.getRemoteAddress().getHostName());
        lblPort.setText("" + serverEntry.getRemoteAddress().getPort());

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @FXML
    void onBtnClose(ActionEvent event) {
        btnClose.getScene().getWindow().hide();
    }
}
