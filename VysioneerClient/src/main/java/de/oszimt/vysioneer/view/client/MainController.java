package de.oszimt.vysioneer.view.client;

import de.oszimt.vysioneer.Main;
import de.oszimt.vysioneer.common.PreferenceStore;
import de.oszimt.vysioneer.common.VyServerEntry;
import de.oszimt.vysioneer.view.BaseController;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class MainController extends BaseController {

    protected ListProperty<VyServerEntry> listProperty;
    @FXML
    private ListView<VyServerEntry> lstServer;
    @FXML
    private SplitMenuButton sbtConnect;
    @FXML
    private MenuItem mnuNew;
    @FXML
    private MenuItem mnuDetails;
    @FXML
    private MenuItem mnuDelete;
    @FXML
    private MenuItem mnuSettings;
    @FXML
    private Text lblUsername;
    // Properties
    private StringProperty username;
    // Observables
    private ObservableList<VyServerEntry> oListServers;

    // All tasks to be done BEFORE fxml loading
    public MainController() {

    }

    public StringProperty usernameProperty() {
        return username;
    }

    public String getUsername() {
        return username.get();
    }

    // All tasks to be done AFTER fxml loading
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Load settings
        PreferenceStore pref = PreferenceStore.getInstance();
        oListServers = pref.getClientSettings().getSortedServerList();
        listProperty = new SimpleListProperty<>();
        listProperty.set(oListServers);

        username = pref.getClientSettings().displayNameProperty();

        // Bind property
        lblUsername.textProperty().bind(username);
        lstServer.itemsProperty().bindBidirectional(listProperty);

    }

    @FXML
    void onMnuDelete(ActionEvent event) {

        VyServerEntry item = lstServer.getSelectionModel().getSelectedItem();

        if (item == null)
            return;

        Optional<ButtonType> result = showAlert("Server löschen",
                "Server löschen: " + item.getDisplayName(),
                "Möchten Sie diesen Server wirklich aus Ihrer Favoriten-Liste löschen?",
                Alert.AlertType.CONFIRMATION);
        if (result.get() == ButtonType.OK) {
            Main.logger.info("Deleting server entry " + item.getDisplayName());
            // Here, we need to work on the "original" list, as the sorted observable does not
            // provide add() or delete() operations. As both lists are linked, changes directly
            // reflect to the sorted list as well.
            PreferenceStore.getInstance().getClientSettings().getServerList().remove(item);
        }
    }

    @FXML
    void onMnuDetails(ActionEvent event) {
        VyServerEntry item = lstServer.getSelectionModel().getSelectedItem();
        if (item == null)
            return;
        showDetailWindow(item);
    }

    private void showDetailWindow(VyServerEntry entry) {
        // Load FXML
        // Create new root, stage and scene
        try {
            // Give the controller access to the main app.
            ShowEntryDetailsController controller = (ShowEntryDetailsController) loadModalWindow("view/ClientShowEntryDetails.fxml", "Server-Details: " + entry.getDisplayName());
            controller.loadServerEntry(entry);
        } catch (IllegalStateException | IOException e) {
            Main.logger.error("Could not load FXML ClientShowEntryDetails.fxml", e);
        }

    }

    @FXML
    void onMnuNew(ActionEvent event) {
        showNewWindow();

    }


    private void showNewWindow() {
        // Load FXML
        // Create new root, stage and scene
        try {
            loadModalWindow("view/ClientNewEntry.fxml", "Neuer Server");
        } catch (IllegalStateException | IOException e) {
            Main.logger.error("Could not load FXML ClientNewEntry.fxml", e);
        }
    }

    @FXML
    void onMnuSettings(ActionEvent event) {
        showSettingsWindow();
    }

    private void showSettingsWindow() {
        // Load FXML
        // Create new root, stage and scene
        try {
            loadModalWindow("view/ClientSettings.fxml", "Einstellungen");
        } catch (IllegalStateException | IOException e) {
            Main.logger.error("Could not load FXML ClientSettings.fxml", e);
        }
    }

}
