package de.oszimt.vysioneer.common;

import java.net.InetSocketAddress;

public class VyClientEntry {

    private String displayName;
    private InetSocketAddress remoteAddress;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public InetSocketAddress getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(InetSocketAddress remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

}
