package de.oszimt.vysioneer.connector;

public enum VyServerState {
    CONNECTED, SHOWING, HANDSHAKE
}
